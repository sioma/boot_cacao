#####################     SIOMA S.A.S    ############################
#                Codigo de inicializacion de nukak
#####################################################################
######               Importacion de librerias                 #######
import os
import socket
from subprocess import PIPE, Popen
import datetime
import urllib.request, urllib.parse, urllib.error
import psycopg2, psycopg2.extensions, psycopg2.extras
import requests

EXECUTE_PATCH = True

######           Datos de version de base de datos            #######
VERSIONDB = 1.0
BITBUCKETSQLREPO = "sudo git clone -b " + str(
    VERSIONDB) + " --depth 1 https://cristian13r:MHqfm9Sm6nPMN9yZEMmD@bitbucket.org/sioma/sql_cacao.git"
SQLREPO = "sql_cacao"
TABLAS = ['backups', 'parametros', 'bultos', 'personas', 'staff', 'lotes', 'estomas', 'validacions', 'wifis']


######           Datos de version de repositorio web          #######
VERSIONWEB = 1.0
BITBUCKETWEBREPO = "sudo git clone -b " + str(
    VERSIONWEB) + " --depth 1 https://cristian13r:MHqfm9Sm6nPMN9yZEMmD@bitbucket.org/sioma/cacaoweb.git"
WEBREPO = "cacaoweb"
WEBPATH = "/var/www/html"

######           Datos de version de repositorio py           #######
VERSIONPY = 1.0
BITBUCKETPYREPO = "sudo git clone -b " + str(
    VERSIONPY) + " --depth 1 https://cristian13r:MHqfm9Sm6nPMN9yZEMmD@bitbucket.org/sioma/nukak_cacao.git"
PYREPO = "nukak_cacao"


######         Declaracion de variables y constantes          #######
online = False
date = datetime.datetime.now()
date = str(date.date())
print("Fecha actual: " + date)
f = open("/firmware/base/Codes/initbascula/info.txt", "r")
parametrosLocales = str.split(f.readline(), ',')
SERIAL = parametrosLocales[0]


######                        Funciones                        #######
def Cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]


def Web_noti(subject, datos, timeout=6, intentos=1):
    print("Subject: ", subject)
    print("Datos a enviar: ", datos)
    SIOMAPPCREDENTIALS = "C0l0mb14S10m4"
    mydata = [('s', SIOMAPPCREDENTIALS), ('subject', subject), ('body', datos)]
    mydata = urllib.parse.urlencode(mydata)
    path = 'https://filtro.siomapp.com/1/correoAlertaStaff'
    header = {"Content-type": "application/x-www-form-urlencoded", "Authorization": "gS8Jdz24bJgevS9loGuiCW9R2IJXLcpdme8nQ8b88No"}
    ok = False
    for i in range(0, intentos):
        i += 1
        if not ok:
            try:
                page = requests.post(path, data=mydata, headers=header, timeout=timeout)
                return page.json()
                ok = True
            except Exception as e:
                print("ERROR EN LA CONEXION A SIOMA: " + repr(e))
                return 0
        else:
            break


def Clonar_repo(repo, repoWeb, emailSubject, emailBody, maxIntentosAct=1, copy=True):
    intentos = 0
    descargar = 1
    while intentos < maxIntentosAct:
        ##### Se borra la carpeta local del repositorio
        try:
            borrar = os.system('sudo rm -R ' + repo)
        except Exception as e:
            msj = "No se borro el repositorio: "
            print(msj + repr(e))
            Web_noti(emailSubject, emailBody + msj)
            break
        print("borrar: ", borrar)

        try:
            descargar = os.system(repoWeb + " >/dev/null 2>&1")
        except Exception as e:
            msj = "No se descargo el repositorio: "
            print(msj + repr(e))
            Web_noti(emailSubject, emailBody + msj)
        print("descargar: ", descargar)

        if descargar != 0:
            break
        if copy:
            try:
                copiar = os.system('sudo cp ' + repo + ' -R /firmware/base/Codes')
            except Exception as e:
                msj = "No se copio el repositorio: "
                print(msj + repr(e))
                Web_noti(emailSubject, emailBody + msj)
            print("Copiar: ", copiar)

        intentos += 1
        if copy:
            if descargar == 0 and copiar == 0:
                return 0
        else:
            if descargar == 0:
                return 0


######################################################################
#######           Bloque de configuraciones del OS           #########
if EXECUTE_PATCH:
    Cmd_line("sudo chmod 755 /firmware/base/Codes/boot_cacao/patch.sh")
    Cmd_line("sudo /firmware/base/Codes/boot_cacao/patch.sh")

######           Validacion de acceso a internet               #######
try:
    socket.setdefaulttimeout(8)
    socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect(("8.8.8.8", 53))
    print('System Online')
    online = True
except Exception as e:
    print('System Offline')
    print("ERROR TESTING INTERNET: " + repr(e))
    online = False

### Actualizacion de hora
hora = Cmd_line("date")
print("Hora sistema: ", hora)
rtc = Cmd_line("sudo hwclock -r")
print("RTC: ", rtc)
if not online:
    print("Sistema sin internet, se usara hora de RTC")
    Cmd_line("sudo hwclock -s")

# ######           Descarga de esquema de base de datos         #######
try:
    os.system('sudo chown pi /firmware -R')
    sqlBackup = os.system("sudo PGPASSWORD='sioma' pg_dump -U postgres -F t estomadb > "
                    "/firmware/base/Codes/" + date)
except Exception as e:
    print("ERROR CREANDO COPIA DE BASE DE DATOS: " + repr(e))
    asunto = "Error de actualizacion de esquema"
    body = "El equipo " + SERIAL + " presenta error al realizar el respaldo de la base de datos."
    Web_noti(asunto, body)

if sqlBackup == 0:
    try:
        drop = 0
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')
        conectar.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ)
        cursor = conectar.cursor()
        cursor.execute("select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';")
        aux = cursor.fetchall()
        if len(aux) > 1:
            tablas = []
            for tabla in aux:
                tablas.append(tabla[0])
            if "parametros" in tablas:
                tablas.remove("parametros")

            dropCommand = "sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -c 'drop table "
            first = True
            for tabla in tablas:
                if first:
                    first = False
                    dropCommand = dropCommand + tabla
                else:
                    dropCommand = dropCommand + ", " + tabla

            dropCommand = dropCommand + "'"
            drop = os.system(dropCommand)
        else:
            drop = 0
    except Exception as e:
        print("ERROR BORRANDO SCHEMA: " + repr(e))
        asunto = "Error de actualizacion de esquema"
        body = "El equipo " + SERIAL + " presenta un error borrando el schema de la base de datos."
        Web_noti(asunto, body)

    if drop == 0:
        try:
            clonar = 0
            clonar = Clonar_repo(SQLREPO, BITBUCKETSQLREPO, "Error de actualizacion de esquema",
                        "El equipo " + SERIAL + " presenta un error descargando el archivo sql.")
        except Exception as e:
            print("ERROR DESCARGANDO .SQL: " + repr(e))

        if clonar == 0:
            try:
                execute = os.system("sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -a -f /firmware/base/Codes/sql_cacao/sql_cacao.sql >/dev/null 2>&1")
                conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')
                conectar.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ)
                cursor = conectar.cursor()
                cursor.execute("select relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';")
                aux = cursor.fetchall()
                tablas = []
                for tabla in aux:
                    tablas.append(tabla[0])
                print(tablas)
                print(TABLAS)
                if len(TABLAS) == len(tablas):
                    br = False
                    checks = []
                    for tablaVersion in TABLAS:
                        for tabla in tablas:
                            if tabla == tablaVersion:
                                checks.append(1)
                                break
                            elif tabla == tablas[len(tablas)-1]:
                                asunto = "Error de actualizacion de esquema"
                                body = "El equipo " + SERIAL + " no paso la validacion de tablas."
                                Web_noti(asunto, body)
                                print("check:SQL:False")
                                br = True
                                break
                        if br:
                            break

                    if len(checks) == len(TABLAS):
                        print("check:SQL:True")
                else:
                    asunto = "Error de actualizacion de esquema"
                    body = "El equipo " + SERIAL + " no paso la validacion de tablas."
                    Web_noti(asunto, body)
                    print("check:SQL:False")

            except Exception as e:
                print("ERROR GENERANDO NUEVO ESQUEMA: " + repr(e))
        else:
            print("check:SQL:False")
    else:
        print("check:SQL:False")
else:
    print("check:SQL:False")

# ######                Descarga repositorio web                #######
try:
    clonar = Clonar_repo(WEBREPO, BITBUCKETWEBREPO, "Error de actualizacion de repositorio web",
                        "El equipo " + SERIAL + " presenta un error descargando el repositorio web", copy=False)
    if clonar == 0:
        try:
            copiar = os.system('sudo cp ' + WEBREPO + ' -R ' + WEBPATH)
        except Exception as e:
            msj = "No se copio el repositorio: "
            print(msj + repr(e))
            Web_noti("Error de actualizacion de repositorio web",
                     "El equipo " + SERIAL + " presenta un error copiando el repositorio web" + msj)
        print("Copiar: ", copiar)

        if copiar == 0:
            restart = os.system('sudo service apache2 restart')

            if restart == 0:
                SiomappCredentials = "C0l0mb14S10m4"
                URL = 'http://localhost/api/version.php'
                try:
                    page = requests.get(URL)
                    versionWeb = float(page.content)
                except Exception as e:
                    versionWeb = 0
                    print("ERROR EN LA CONEXION A SIOMA: " + repr(e))

                if versionWeb == VERSIONWEB:
                    print("check:WEB:True")
                else:
                    print("check:WEB:False")
            else:
                print("check:WEB:False")
        else:
            print("check:WEB:False")
    else:
        print("check:WEB:False")

except Exception as e:
    print("ERROR ACTUALIZANDO CODIGO WEB: " + repr(e))

######                Descarga repositorio py                 #######
try:
    clonar = Clonar_repo(PYREPO, BITBUCKETPYREPO, "Error de actualizacion de repositorio py",
                        "El equipo " + SERIAL + " presenta un error descargando el repositorio py")
    if clonar == 0:

        import sys
        sys.path.insert(0, '/firmware/base/Codes/nukak_cacao')
        import credentials
        versionPy = credentials.Get_Estoma_Info()
        versionPy = versionPy[1]

        if versionPy == VERSIONPY:
            print("check:PY:True")
        else:
            print("check:PY:False")
    else:
        print("check:PY:False")
except Exception as e:
    print("ERROR ACTUALIZANDO CODIGO WEB: " + repr(e))
    asunto = "Error de actualizacion de py"
    body = "El equipo " + SERIAL + " presenta error al realizar la actualizacion de py."
    Web_noti(asunto, body)
